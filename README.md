# Vue.js Demo Application

> A Vue.js project based on the [Vuetify Webpack Template](https://github.com/vuetifyjs/webpack)

Included core libraries:

- [Vue Router](https://router.vuejs.org/)
- [Vuex.js](https://vuex.vuejs.org/)

Extra libraries:

- [Vuetify](https://vuetifyjs.com/) - Material Component Framework
- [Axios](https://github.com/axios/axios) - Promise based HTTP client for the browser and node.js
- [Vue Axios](https://github.com/imcvampire/vue-axios) - A small wrapper for integrating axios to Vuejs
- [Vue Auth](https://github.com/websanova/vue-auth) - JWT-auth library for Vue.js
- [Font Awesome](https://fontawesome.com/) - Icon set and toolkit

Features:

- Authentication
- Secured routes
- Menu's
- Breadcrumbs
- Expansion panels
- Spinner while loading data

## Vue Auth

This one is a bit tricky to setup. It has certain expactations about the server API, which are only documented in the source code. In this App the Bearer driver of Vue Auth is used. It expects the token is returned in the Authorization header and requires Access-Control-Expose-Headers set to Authorization.^[This is actually a CORS requirement. The server is telling the browser which headers are allowed to exposed. See [here](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Expose-Headers) for more information.]

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
