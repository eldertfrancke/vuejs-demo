import Vue from 'vue'
import Vuex from 'vuex'
// Generic http-client
import axios from 'axios'
// Vue-wrapper for Axios
import VueAxios from 'vue-axios'

// Inject Axios in Vue
Vue.use(VueAxios, axios)
Vue.use(Vuex)

const attendee = {
  _id: 1,
  name: 'Deel Nemer'
}

const skillRecords = [{
  _id: 1,
  skillId: 1
},
{
  _id: 2,
  skillId: 2
},
{
  _id: 3,
  skillId: 3
},
{
  _id: 4,
  skillId: 4
},
{
  _id: 5,
  skillId: 5
}
]

const skills = [{
  _id: 1,
  name: 'Skill 1',
  description: 'This is a description of skill 1'
},
{
  _id: 2,
  name: 'Skill 2',
  description: 'This is a description of skill 2'
},
{
  _id: 3,
  name: 'Skill 3',
  description: 'This is a description of skill 3'
},
{
  _id: 4,
  name: 'Skill 4',
  description: 'This is a description of skill 4'
},
{
  _id: 5,
  name: 'Skill 5',
  description: 'This is a description of skill 5'
}
]

const store = new Vuex.Store({
  state: {
    courseTypes: undefined,
    groups: undefined,
    attendees: undefined,
    attendee,
    skillRecords,
    skills
  },
  getters: {
    skillReport (state) {
      const report = state.skills.map(skill => {
        skill.value = true
        return skill
      })
      return report
    }
  },
  mutations: {
    setCourseTypes (state, courseTypes) {
      state.courseTypes = courseTypes
    },
    setGroups (state, courses) {
      state.groups = courses
    },
    setAttendees (state, attendees) {
      state.attendees = attendees
    }
  },
  actions: {
    getCourseTypes: ({
      commit
    }) => {
      Vue.axios.get('coursetypes')
        .then(res => {
          commit('setCourseTypes', res.data)
        })
    },
    getGroups: ({
      commit
    }, coursetypeID) => {
      Vue.axios.get(`courses`, {
        courseType: coursetypeID
      })
        .then(res => commit('setGroups', res.data))
    },
    getAttendees: ({
      commit
    }, courseID) => {
      Vue.axios.get(`courses/${courseID}/attendees`)
        .then(res => commit('setAttendees', res.data))
    }
  }
})

export default store
