// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

// Generic http-client
import axios from 'axios'
// Vue-wrapper for Axios
import VueAxios from 'vue-axios'

// Jwt Auth library for Vue.js
import VueAuth from '@websanova/vue-auth'

// Material Component Framework
import Vuetify from 'vuetify'

// Import App component
import App from './App'
// import router component to inject in Vue
import router from './router'
// Import store to inject in Vue
import store from '@/store'

// Import vuetify css
import 'vuetify/dist/vuetify.min.css'

// Inject Axios in Vue
Vue.use(VueAxios, axios)

// Required statement for VueAuth
Vue.router = router

Vue.use(VueAuth, {
  auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
  http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
  router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
  loginData: {
    url: '/users/authenticate',
    fetchUser: false
  },
  fetchData: {
    // FIXME: this must be fixed at the api
    enabled: false
  },
  refreshData: {
    // FIXME: this must be fixed at the api
    enabled: false
  }
})

// Inject Vuetify in Vue
Vue.use(Vuetify)

Vue.config.productionTip = false

// Configure base url
Vue.axios.defaults.baseURL = 'http://localhost:3000/api'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {
    App
  },
  template: '<App/>'
})
