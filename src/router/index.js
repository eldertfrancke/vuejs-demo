import Vue from 'vue'
import Router from 'vue-router'
import CourseList from '@/components/CourseList'
import GroupList from '@/components/GroupList'
import AttendeeList from '@/components/AttendeeList'
import Progress from '@/components/Progress'
import Login from '@/components/Login'

Vue.use(Router)

const router = new Router({
  routes: [{
    path: '/',
    redirect: '/courses'
  }, {
    path: '/courses',
    name: 'CourseList',
    component: CourseList,
    meta: {
      auth: true
    }
  }, {
    path: '/courses/:courseID/groups',
    name: 'GroupList',
    props: true,
    component: GroupList,
    meta: {
      auth: true
    }

  }, {
    path: '/courses/:courseID/groups/:groupID',
    name: 'AttendeeList',
    props: true,
    component: AttendeeList,
    meta: {
      auth: true
    }

  }, {
    path: '/courses/:courseID/groups/:groupID/attendees/:attendeeID/progress',
    name: 'Progress',
    component: Progress,
    meta: {
      auth: true
    }

  }, {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      auth: false // If the user is logged in then this route will be unavailable. Useful for login/register type pages to be unaccessible once the user is logged in.
    }
  }]
})

export default router
